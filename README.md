# README #
Hi! If you are having trouble pulling this repository, I attached a tinyurl link in my application right next to my bitbucket link, where you can unzip the files and put them in a compiler. Otherwise, you can also copy paste the files from bitbucket into a compiler. Thanks!

This is a project that I completed in my APCS A Class in the 2020-2021 school year as a first semester final project. 

Hello! This project is a game of Spider Solitaire, writen in Java! You can pull this repository or copy-paste each file into a compiler like Eclipse and run it! Essentially, when run, a text based game of spider solitaire would show up, with some instructions on how to play. Those who do not know how to play can simply google Spider Solitaire and can read/watch videos on how to play. 