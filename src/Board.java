import java.util.*;
import javax.swing.*;
import java.io.*;
import java.awt.EventQueue;
import javax.swing.JFileChooser;
import java.lang.reflect.InvocationTargetException;



public class Board
{   
 
    // Attributes
    /** The array of decks */
    Deck[] decks; //contains the stacks
   /** The amount of stacks */
    int amountOfStacks;
    
    /**
     *  Sets up the Board and fills the stacks and draw pile from a Deck
     *  consisting of numDecks Decks.  Here are examples:
     *  
     *  # numDecks     #cards in overall Deck
     *      1          13 (all same suit)
     *      2          26 (all same suit)
     *      3          39 (all same suit)
     *      4           52 (all same suit)
     *      
     *  Once the overall Deck is built, it is shuffled and half the cards
     *  are placed as evenly as possible into the stacks.  The other half
     *  of the cards remain in the draw pile.
     */    
    public Board(int numStacks, int numDecks) { //4, 4
   
        decks = new Deck[numStacks+1];
        amountOfStacks= numStacks;
        //Combineddecks is the big combined deck of cards consisting of 13*numDecks cards
        Deck combineddecks = new Deck();
        for(int m=0; m<numStacks+1; m++){
            
            decks[m] = new Deck();
            
            
        }
        //uploading the combindeddecks array with cards
          for(int i=1; i<=numDecks; i++){
             String[] symbols = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"};
             int[] values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

               for(int j=0; j<symbols.length; j++){
                       combineddecks.add(new Card(symbols[j], values[j]));
                }
               }
               
            //setting all cards face up
       combineddecks.setAllCardsFaceDown();
       
       combineddecks.shuffle();//shuffled 52 cards
       int totalNumberOfCards = numDecks*13; //52 cards

       int halfDeck = totalNumberOfCards/2; //26 cards

       int roughAmountOfCards = halfDeck/numStacks; // 6 cards

       int amountOfCardsRem = halfDeck-roughAmountOfCards*numStacks; //2
       int counter = 0;
       for(int i=1; i<=roughAmountOfCards; i++){
           for(int k=0; k<numStacks; k++){
               //adding cards
               decks[k].add(combineddecks.getAtIndex(counter));
               counter+=1;
           }

       }
      if(amountOfCardsRem>0){ 
       for(int m=0; m<amountOfCardsRem; m++){
            //putting the remainiign cards
           decks[m].add(combineddecks.getAtIndex(counter));
           counter++;
       }
       
        
        
    }
    for(int i=counter; i<totalNumberOfCards; i++){
        //adding cards to the draw pile
        decks[numStacks].add(combineddecks.getAtIndex(counter));
        counter ++;
    }
   decks[numStacks].setAllCardsFaceDown();
   
   
   for(int i=0; i<numStacks; i++){
       //setting last card face up
       decks[i].setLastCardFaceUp();
       
    }
   
   //TESTINGS
    
   /* Second Test (b)- Passed
    decks[0].add(new Card("A", 1));
    decks[0].add(new Card("2", 2));
    decks[0].add(new Card("3", 3));
    decks[0].add(new Card("4", 4));
    decks[0].add(new Card("5", 5));
    decks[0].add(new Card("6", 6));
    decks[0].add(new Card("7", 7));
    decks[0].add(new Card("8", 8));
    decks[0].add(new Card("9", 9));
    decks[0].add(new Card("10",10));
    decks[0].add(new Card("J", 11));
    decks[0].add(new Card("Q", 12));
    decks[0].add(new Card("K", 13));
    clear(1);
    */
    
  //  clear(1);
  /* Test 5a - PASSED
  decks[0].removeAllElements();
  decks[0].add(new Card("A", 1));
    decks[0].add(new Card("2", 2));
    decks[0].add(new Card("3", 3));
    decks[0].add(new Card("4", 4));
    decks[0].add(new Card("5", 5));
    decks[0].add(new Card("6", 6));
    decks[0].add(new Card("7", 7));
    decks[0].add(new Card("8", 8));
    decks[0].add(new Card("9", 9));
    decks[0].add(new Card("10",10));
    decks[0].add(new Card("J", 11));
    decks[0].add(new Card("Q", 12));
    decks[0].add(new Card("K", 13));
      clear(1);
    */
   
   //Test 5c- PASSED, reported an error and did not modify
   /*
   decks[0].removeAllElements();
   decks[0].add(new Card("A", 1));
    decks[0].add(new Card("2", 2));
    decks[0].add(new Card("3", 3));
    decks[0].add(new Card("4", 4));
    decks[0].add(new Card("5", 5));
    decks[0].add(new Card("6", 6));
    decks[0].add(new Card("7", 7));
    decks[0].add(new Card("8", 8));
    decks[0].add(new Card("9", 9));
    decks[0].add(new Card("10",10));
    decks[0].add(new Card("J", 11));
    decks[0].add(new Card("Q", 12));
    decks[0].add(new Card("K", 13));
    decks[0].add(new Card("A", 1));
    clear(1);
    */
   //Test 5D: PASSED-reported an error as desired 
   /*
   decks[0].removeAllElements();
   decks[0].add(new Card("K", 13));
    decks[0].add(new Card("Q", 12));
     decks[0].add(new Card("J", 11));
        decks[0].add(new Card("10",10));
         decks[0].add(new Card("9", 9));
           decks[0].add(new Card("8", 8));
           decks[0].add(new Card("7", 7));
               decks[0].add(new Card("6", 6));
                decks[0].add(new Card("5", 5));
                   decks[0].add(new Card("4", 4));
                   decks[0].add(new Card("3", 3));
                    decks[0].add(new Card("2", 2));
   decks[0].add(new Card("A", 1));
    
   clear(1);
    
   */
  
  //Test 5E: Passed, reported error as desired
  /*
  decks[0].removeAllElements();
   decks[0].add(new Card("K", 13));
    decks[0].add(new Card("Q", 12));
     decks[0].add(new Card("J", 11));
        decks[0].add(new Card("10",10));
         decks[0].add(new Card("9", 9));
           decks[0].add(new Card("8", 8));
           decks[0].add(new Card("7", 7));
               decks[0].add(new Card("6", 6));
                decks[0].add(new Card("5", 5));
                   decks[0].add(new Card("4", 4));
                   clear(1);
                   */
    //Test 5F: PASSED 
    /*
    decks[0].removeAllElements();
    Card cardd = new Card("K", 13);
    cardd.setFaceUp(true);
    
   decks[0].add(cardd);
    decks[0].add(new Card("Q", 12));
     decks[0].add(new Card("J", 11));
        decks[0].add(new Card("10",10));
         decks[0].add(new Card("9", 9));
           decks[0].add(new Card("8", 8));
           decks[0].add(new Card("7", 7));
               decks[0].add(new Card("6", 6));
                decks[0].add(new Card("5", 5));
                   decks[0].add(new Card("4", 4));
                   decks[0].add(new Card("3", 3));
                    decks[0].add(new Card("2", 2));
   decks[0].add(new Card("A", 1));
   clear(1);
        */    
       
      /* Make Move Logic TEST 1: PASEED (TESTING THE MAKE MOVE LOGIC)
       decks[0].removeAllElements();
       decks[0].add(new Card("Q", 12));
     decks[0].add(new Card("J", 11));
        decks[0].add(new Card("10",10));
         decks[0].add(new Card("9", 9));
         decks[0].setAllCardsFaceUp();
         
         decks[1].removeAllElements();
         decks[1].add(new Card("8", 8));
           decks[1].add(new Card("7", 7));
               decks[1].add(new Card("6", 6));
                decks[1].add(new Card("5", 5));
                decks[1].setAllCardsFaceUp();
          makeMove("8", 2, 1);//from decks[1] to decks[0]
          */
         
         /* Test 2: It printed out desired output
         decks[0].removeAllElements();
       decks[0].add(new Card("Q", 12));
     decks[0].add(new Card("J", 11));
        decks[0].add(new Card("10",10));
         decks[0].add(new Card("6", 6));
         decks[0].setAllCardsFaceUp();
         
         decks[1].removeAllElements();
         decks[1].add(new Card("8", 8));
           decks[1].add(new Card("7", 7));
               decks[1].add(new Card("6", 6));
                decks[1].add(new Card("5", 5));
                decks[1].setAllCardsFaceUp();
          makeMove("8", 2, 1);
         
        */ 
       /* Another TesT: Setting one of the cards face down and seeing what happens
       decks[0].removeAllElements();
       decks[0].add(new Card("Q", 12));
     decks[0].add(new Card("J", 11));
        decks[0].add(new Card("10",10));
         decks[0].add(new Card("9", 9));
         decks[0].setAllCardsFaceUp();
         
         
         
         decks[1].removeAllElements();
         decks[1].add(new Card("8", 8));
           decks[1].add(new Card("7", 7));
               decks[1].add(new Card("6", 6));
                decks[1].add(new Card("5", 5));
                decks[1].setAllCardsFaceUp();
                
                decks[1].setCardAtIndexFaceDown(0);
                
          makeMove("8", 2, 1);
         */ 
        
}
    /**
     *  Moves a run of cards from src to dest (if possible) and flips the
     *  next card if one is available.
     */
    public void makeMove(String symbol, int src, int dest) {
       
        boolean a=false;
        int counter = decks[src-1].getSize()-1;
        boolean b = false;
        int bounter = decks[src-1].getSize()-1;
        while(b==false){
            //checking if each is consecutive and symbols match
            if(decks[src-1].getAtIndex(bounter).getSymbol().equals(symbol)){
                b=true;
                break;
                //
            }
            else {
               if(bounter!=0){
                 bounter --;  
                   
                }
                else {
                    
                   System.out.println("Sorry counter not found");
                   return;
                }
                
            }
        }
        boolean ok= true;
        //using compare to to see if they are consecutive
        for(int jj=bounter; jj<decks[src].getSize(); jj++){
            if(jj!=decks[src-1].getSize()-1 && decks[src-1].getAtIndex(jj).compareTo(decks[src-1].getAtIndex(jj+1))==1){
                continue;
                
            }
            else if(jj==decks[src-1].getSize()-1){
                ok=true;
                break;
                //logic
            }
            else {
             ok = false;
             break;
            }
            
        }
        if(ok==false){
           System.out.println("Not consecutive in Stack");
           return;
            
        }
        else if(decks[src-1].getAtIndex(bounter).isFaceUp()==false){
            System.out.println("Symbol Card not found in stack");
            return;
            
        }
        else {
            if(decks[dest-1].getSize()==0){
                for(int f=bounter; f<decks[src-1].getSize(); f++){
                    //adding card
                    decks[dest-1].add(decks[src-1].getAtIndex(f));
                    
                }
                for(int m = decks[src-1].getSize()-1; m>=bounter; m--){
                    //removing card that from other pile
                    decks[src-1].removeAtCertainIndex(m);
                }
                if(decks[src-1].getSize()>=1){
                    //setting face up
                    decks[src-1].setCardAtIndexFaceUp(decks[src-1].getSize()-1);
                }
                
            }
            else {//logic
                if(decks[dest-1].getAtIndex(decks[dest-1].getSize()-1).compareTo(decks[src-1].getAtIndex(bounter))==1){
                    for(int f=bounter; f<decks[src-1].getSize(); f++){
                    //adding card
                    decks[dest-1].add(decks[src-1].getAtIndex(f));
                    
                }
                for(int m = decks[src-1].getSize()-1; m>=bounter; m--){
                    
                    decks[src-1].removeAtCertainIndex(m);
                }
                if(decks[src-1].getSize()>=1){
                    
                    decks[src-1].setCardAtIndexFaceUp(decks[src-1].getSize()-1);
                }
                    
                }
                else {
                    //printing
                   System.out.println("Not a valid move"); 
                    
                }
                
                
                
            }
            
            
            
        }
    }

    /** 
     *  Moves one card onto each stack, or as many as are available
     */
    public void drawCards() {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 5 *** */
       if(isStacksEmpty()==true){
           //logic using method
           System.out.println("Cannot draw cards unless all stacks are non-empty");
           
        }
        //conditional logic
       else if(decks[amountOfStacks].getSize() >=amountOfStacks){
            for(int f=0; f<amountOfStacks; f++){
                Card c = decks[amountOfStacks].getAtIndex(f);
           
                decks[f].add(c);
                 c.setFaceUp(true);
                
            }
            
            
         
        for(int i=0; i<amountOfStacks; i++){
            //removing card
            decks[amountOfStacks].removeAtCertainIndex(0);
        }
        decks[amountOfStacks].setAllCardsFaceDown();
        
    }
    else {
        //;ogic using for loop
        for(int f=0; f<decks[amountOfStacks].getSize(); f++){
            Card c = decks[amountOfStacks].getAtIndex(f);
           
            decks[f].add(c);
         
            c.setFaceUp(true);
        }
int x = decks[amountOfStacks].getSize();
        for(int g=0; g<x; g++){
            decks[amountOfStacks].removeAtCertainIndex(0);
            
            //removing card
        }
         decks[amountOfStacks].setAllCardsFaceDown();
        
    }
        
    }

    /**
     *  Returns true if all stacks and the draw pile are all empty
     */ 
    public boolean isEmpty() { //checks using helper method in deck
      
        boolean a= true;
        for(int i=0; i<decks.length; i++){
            if(decks[i].isEmpty()==false){
                return false;
                
            }
            
        }
        return true;
        
        
    }
    /**
     * Returns true or false if all the stacks are empty(does not check the draw pile
     */
    public boolean isStacksEmpty(){
        boolean a= true; //using for loop
        for(int i=0; i<decks.length-1; i++){
            if(decks[i].isEmpty()==false){
                return false;
                
            }
            
        }
        return true;
        
    }

    /**
     *  If there is a run of A through K starting at the end of sourceStack
     *  then the run is removed from the game or placed into a completed
     *  stacks area.
     *  
     *  If there is not a run of A through K starting at the end of sourceStack
     *  then an invalid move message is displayed and the Board is not changed.
     *  
     *  @param sourceStack this is the stack number you want to clear(positive int only)
     */
    public void clear(int sourceStack) {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 5 *** */
        if(sourceStack>amountOfStacks ){
            //clears if possible
            System.out.println("Error, value input out of bounds");
            
        }
        else if(decks[sourceStack-1].getSize()<13){
            System.out.println("Invalid Move");
            
        }
        else {
           int indexx = sourceStack-1;
           int size = decks[indexx].getSize();
          boolean a = true;
           
            for(int i=1; i<=13; i++){
                if(decks[indexx].getAtIndex(size-i).isFaceUp()==false &&a==true){
                    
                    System.out.println("Invalid Move");
                    a=false;
                    
                    
                }
                
            }
            
           //logic 
             if(a==true && decks[indexx].getAtIndex(size-1).getSymbol()=="K" && decks[indexx].getAtIndex(size-2).getSymbol()=="Q" && decks[indexx].getAtIndex(size-3).getSymbol()=="J" && decks[indexx].getAtIndex(size-4).getSymbol()=="10" && decks[indexx].getAtIndex(size-5).getSymbol()=="9" && decks[indexx].getAtIndex(size-6).getSymbol()=="8" && decks[indexx].getAtIndex(size-7).getSymbol()=="7" && decks[indexx].getAtIndex(size-8).getSymbol()=="6" && decks[indexx].getAtIndex(size-9).getSymbol()=="5" && decks[indexx].getAtIndex(size-10).getSymbol()=="4" && decks[indexx].getAtIndex(size-11).getSymbol()=="3" && decks[indexx].getAtIndex(size-12).getSymbol()=="2" && decks[indexx].getAtIndex(size-13).getSymbol()=="A"){
                   for(int i=1; i<=13; i++){
                        decks[indexx].removeAtCertainIndex(size-i);
                        
                         //then removes
                    }   
                    
                    
                }
                
                else {
                  if(a==true){  
                  System.out.println("Invalid Move");  //invalid print
                }
                else {
                }
                }
            
            
        }
        }
        
    

    /**
     * Prints the board to the terminal window by displaying the stacks, draw
     * pile, and done stacks (if you chose to have them)
     */
    public void printBoard() {
        /* *** TO BE IMPLEMENTED IN ACTIVITY 4 *** */
        System.out.println("Stacks:");
        //printing board using deck helper method
       for(int amount=0; amount<amountOfStacks; amount+=1){
           int x  = amount+1;
           System.out.print(x + ": ");
           decks[amount].printDeck();
           System.out.println();
           
           
        }
        System.out.println("Draw Pile:");
        decks[amountOfStacks].printDeck();
        System.out.println();
        
    }
    
    public void saveToTextFile(){
      
   
    
    
    
    
    try {
            EventQueue.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                       JFileChooser chooser = new JFileChooser(".");
      
    //  File apple = chooser.getSelectedFile();
    File apple;
      
          chooser.showSaveDialog(null);
          apple = chooser.getSelectedFile();
    if(apple==null){
        
        
        System.out.println("You canceled the file");
        return ;
        
    } 
    
      try{
      FileWriter banana = new FileWriter(apple);
       
                        for(int i=0; i<amountOfStacks+1; i++){
                          
          String a = decks[i].returnDeck() + "\n";
          banana.write(a, 0, a.length());
        
         
        }
            banana.close();
            return;
    }catch(Exception e){
        
        
    }
    return;
                    }
                });
        }
        catch (InterruptedException e) {
           System.out.println("Error: " + e.getMessage());
        }
        catch (InvocationTargetException e) {
System.out.println("Error: " + e.getMessage());
        }


    }
    

   
    

    public void restoreFromTextFile(){
       
        try {
            EventQueue.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                     // Insert your JFileChooser and file
// saving code here 
        JFileChooser chooser = new JFileChooser(".");
          chooser.showOpenDialog(null);
      File apple = chooser.getSelectedFile();
      try{
        Scanner s = new Scanner(apple);
        int i=0;
        while(s.hasNextLine()){
//getting filewriter input
            String sa = s.nextLine();
           decks[i] = new Deck(sa);
           
            i++;
        }
        s.close();
        return;
    }catch(Exception e){
        
        
    }
    return;
                    }
                });
        }
        catch (InterruptedException e) {
            //exit
           System.out.println("Error: " + e.getMessage());
        }
        catch (InvocationTargetException e) {
System.out.println("Error: " + e.getMessage());
        }

    
    
    
    }
    
}