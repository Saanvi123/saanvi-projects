

public class CardTester
{
    public static void main(String[] args) {
        
        //Creating the first card
        Card c1 = new Card("A", 1);
        //testing the get symbol
        System.out.println(c1.getSymbol());
        //testing the get value
        System.out.println(c1.getValue());
        
        //setting it as face up
        c1.setFaceUp(true);
        //testing isfaceup method
        System.out.println(c1.isFaceUp());
        //testing the to string
        System.out.println(c1.toString());
        //creating another card
        Card c2  = new Card("2", 2);
        //checking if c1 equals c2
        System.out.println(c1.equals(c2));
        //testing the compareto method
        System.out.println(c1.compareTo(c2));
        
        //setting c2 to be facedown
        c2.setFaceUp(false);
        //testing the tostring method
        System.out.println(c2.toString());
        //creating a third card
        Card c3 = new Card("3", 3);
        //testing get value method
        System.out.println(c3.getValue());
        
        
    }
}
