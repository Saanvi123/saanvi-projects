import java.util.*;

public class Deck
{
    /* *** TO BE IMPLEMENTED IN ACTIVITY 3 *** */
    /**The arraylist that will be used to store the cards*/
    private  ArrayList<Card> list = new ArrayList<Card>();
    private String deckcontents;
    //Add a constructor to your Deck class that initializes the Deck to an empty state. 
   /**
    * This is the empty constructor
    */
    public Deck(){
        
        
        
    }
    
    /**
     * This is the constructor method for deck, taking in an string and setting the deck
     */
    public Deck(String answer){
     
        deckcontents = answer;
       
        int i=0;
        while(i<deckcontents.length()){
           int end = 0;
            String str = "";
            for(int j=i; j<deckcontents.length(); j++){
                
                if((answer.charAt(j))!= ' '){
                    str += answer.charAt(j);
                    
                    continue;
                }
                else{
                    
                    end = i;
                    break;
                    
                }//i to j-1
                

                
            }
            
          String okcool = "";
          int endd = 0;
         
            for(int m=end+2; m<deckcontents.length(); m++){
                
                if(Character.isDigit(answer.charAt(m))){
                    okcool += answer.charAt(m);
               
                    
                }
                else{
                    
                    endd = m;
                    break;
                }
                
                
            }
        
        
            
        int x = Integer.parseInt(okcool);
            
            String ban= "";
            int bend = 0;
            for(int t = endd+1; t<deckcontents.length(); t++){
                
               if((deckcontents.charAt(t))!= ' '){
                  
                   ban +=deckcontents.charAt(t);
                }
                else{
                    bend = t;
                    break;
                    
                }//i to j-1
                
                
            }
            
          
            boolean value;
            if(ban.equals("true")){
                value = true;
                
                
            }
            else if(ban.equals("false")) value = false;
            else value = true;
  
          Card c  = new Card(str, x);
           c.setFaceUp(value);
            
          list.add(c);
           
       
            i=bend+1;
            
            
            
            
            
        }
      
    }
    /**
     * This is the shuffle method. It shuffles the cards.
     */
    public void shuffle(){
        
        ArrayList<Card> listcopy = new ArrayList<Card>();
        
        for(int i=0; i<list.size(); i++){
            //copy of the arraylist
            listcopy.add(list.get(i));
            
            
        }
        
        for(int i=0; i<list.size(); i++){
            //shuffles the cards,chooses random number and removes that
            Random random = new Random();
            int x = random.nextInt(listcopy.size());
            list.remove(i);
            
            list.add(i, listcopy.get(x));
            
            listcopy.remove(x);
            
            
        }
        
        
    }
    /**
     * This is the add method. It adds a card to the end of the arraylist
     * @param c This is the reference to a new card that you want to be added to the end
     */
    public  void add(Card c){
        //adds a card
        list.add(c);
        
    }
    
    /**
     * This is the method that removes a card at a certain index
     * @param index this is the index at which we remove a card
     */
    public void removeAtCertainIndex(int index){
        if(index >=list.size()){
           System.out.println("Index is out of bounds"); 
        }
        list.remove(index);
        
        
    }
    /**
     * This is the add method that adds a card at a specific index.
     */
    public void addAtSpecificIndex(Card c, int index){
        //adds a card ar an index
        
        list.add(index, c);
        
        
    }
    
   
    
    
    
    /**
	This method returns the current deck. In each parenthesis, we can see the symbol and value of each card.
	@return The string of deck of cards(gives the symbol and value of it).
	 */
	@Override
    public String toString() {
       
        String deck = "";
        for(int i=0; i<list.size(); i++){
            //returns some info about the card
            Card c = list.get(i);
            deck += "(The symbol is " + c.getSymbol() + ": The value is " + c.getValue() + ")";
            
            
        }
        return deck;
        
    }
    /**
     *This method prints out the deck in a certain format 
     * 
     */
    public  void printDeck(){
       if(list.size()==0){
          System.out.print("[ ]"); 
           
        }
        else{
        System.out.print("[");
      
        for(int i=0; i<list.size(); i++){
            if(i!=list.size()-1){
               
                 System.out.print(list.get(i) + ", ");
                
                
            }
            else {
                
                System.out.print(list.get(i) + "]");
                
            }
            
        }
       
    }
        
    }
    /**
     * This method checks if the deck is empty
     * @return true or false
     */
    public boolean isEmpty(){
        
        if(list.size()==0){
            return true;
        }
        else return false;
    }
    /**
     * This method gets the Card at a certain index
     * @param index The card index you wish to acess
     * @return The card at the index that you put in
     */
    public  Card getAtIndex(int index){
        return list.get(index);
    }
    /**
     * Tis mehtod returns the size of the deck
     * @return the size of the deck.
     */
    public int getSize(){
       return list.size(); 
        
    }
    /**
     * This method sets all the cards of the arraylist face up
     */
    public void setAllCardsFaceUp(){
        
        for(int i=0; i<list.size(); i++){
            
            list.get(i).setFaceUp(true);
            
        }
        
    }
    /**
     * This method sets all cards face down
     */
    public void setAllCardsFaceDown(){
        for(int i=0; i<list.size(); i++){
            
            list.get(i).setFaceUp(false);
            
        }
        
    }
    /**
     * This method removes all elements from arraylist
     */
    public void removeAllElements(){
        for(int i=list.size()-1 ; i>=0; i--){
            
            list.remove(i);
        }
        
    }
    /**
     * This method sets a card at a certain index face up
     * @param index This is the index of the card one wishes to set face up
     */
    public void setCardAtIndexFaceUp(int index){
        
        list.get(index).setFaceUp(true);
        
    }
    /**
     * This method sets a card at a certain index face down
     * @param index This is the index of the card one wishes to set face down
     */
    public void setCardAtIndexFaceDown(int index){
        list.get(index).setFaceUp(false);
        
    }
    /**
     * This method sets the last card of the deck face up.
     */
    public void setLastCardFaceUp(){
        
        list.get(list.size()-1).setFaceUp(true);
        
    }
        
        /**
         * This returns the Deck in a string formate seperated by spaces
         * @return String of the deck
         */
    
    public  String returnDeck(){
        
        //a method that contains all of the information needed 
        String answer = "";
        
        for(Card c: list){
            
            answer += c.getSymbol() + " ";
            answer += c.getValue() + " ";
            answer += c.isFaceUp() + " ";
            
            
        }
        return answer;
        
    }
    
    
}
