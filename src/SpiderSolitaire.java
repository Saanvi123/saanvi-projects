import java.util.Scanner;
import java.util.*;


public class SpiderSolitaire
{
    /** Number of stacks on the board **/
    public final int NUM_STACKS = 7;

    /** Number of complete decks used in this game.  A 1-suit deck, which is the
     *  default for this lab, consists of 13 cards (Ace through King).
     */
    public final int NUM_DECKS = 4;

    /** A Board contains stacks and a draw pile **/
    private Board board;

    /** Used for keyboard input **/
    private Scanner input;
    
    

    public SpiderSolitaire()
    {
        // Start a new game with NUM_STACKS stacks and NUM_DECKS of cards
        board = new Board(NUM_STACKS, NUM_DECKS);
        input = new Scanner(System.in);
    }

    /** Main game loop that plays games until user wins or quits **/
    public void play() {

        board.printBoard();
        boolean gameOver = false;

        while(!gameOver) {
            System.out.println("\nCommands:");
            System.out.println("   move [card] [source_stack] [destination_stack]");
            System.out.println("   draw");
            System.out.println("   clear [source_stack]");
            System.out.println("   restart");
            System.out.println("   save");
            System.out.println("   load");
            System.out.println("   help");
            System.out.println("   quit");
            System.out.print(">");
        
            String command = input.nextLine();//read a whole line, new scanner
            
            if (command.length()>=4 && command.substring(0, 4).equals("move")) {
                /* *** TO BE MODIFIED IN ACTIVITY 5 *** */
                 
             try{
                 
                 /*
                    String symbol = input.next();
                    int sourceStack = input.nextInt();
                    int destinationStack = input.nextInt();*/
                    String dommand = command.substring(5);
                    String symbol = dommand.charAt(0) + "";
                    
                    int endindex = 0;
                    String index = "";
                    for(int i=2; i<dommand.length(); i++){
                        
                        if(Character.isDigit(dommand.charAt(i))){
                            index += dommand.charAt(i);
                        }
                        
                        else {
                            endindex = i;
                           break; 
                            
                        }
                        
                    }
                    int sourceStack = Integer.parseInt(index);
              
                    String f = "";
                    
                    for(int i=endindex+1; i<dommand.length(); i++){
                        if(Character.isDigit(dommand.charAt(i))){
                            f+= dommand.charAt(i);
                            
                        }
                        else {
                            break;
                            
                        }
                        
                        
                    }
                    int destinationStack = Integer.parseInt(f);
          
                 board.makeMove(symbol, sourceStack , destinationStack );
              
                }catch(Exception r){
                    System.out.println("Invalid command try again"); 
                   
              
                }
                 
                }
            else if (command.equals("draw")) {
                board.drawCards();
            }
            else if (command.length()>=5 && command.substring(0, 5).equals("clear")) {
                /* *** TO BE MODIFIED IN ACTIVITY 5 *** */
                try{
                    int sourceStack = Integer.parseInt(command.substring(6));
                     board.clear(sourceStack);
                    }catch(Exception e){
                        System.out.println("Invalid Command Try Again");
                        
                        
                    }
            }
            else if (command.equals("restart")) {
                board = new Board(NUM_STACKS, NUM_DECKS);
            
                
            }
            else if (command.equals("quit")) {
                System.out.println("Goodbye!");
                System.exit(0);
            }
            else if(command.equals("save")){
  //save
                board.saveToTextFile();
         
                
            }
            else if(command.equals("load")){
              //restore  
              board.restoreFromTextFile();
         

            }
            else if(command.equals("help")) {
            	System.out.println("This is a game of Spider Solitaire");
            	System.out.println("In this game, we will use only one suit of cards");
            	System.out.println("We will have seven stacks of cards in this game");
            	System.out.println("The object of this game is to build a column such that it has a descending sequence from K to A");
            	System.out.println("Please read http://www.solitairecity.com/Help/Spider.shtml for some more detail");
            	System.out.println("Drawing  places a new card in each pile");
            	
            }
            else {
                System.out.println("Invalid command.");
            }
            board.printBoard();
                      
            // If all stacks and the draw pile are clear, you win!
            if (board.isEmpty()) {
                gameOver = true;
            }
        }
        System.out.println("Congratulations!  You win!");
    }
}
